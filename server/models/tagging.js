module.exports = (sequelize, DataTypes) => {
  const Tagging = sequelize.define('Tagging', {
    tagId : {
      type: DataTypes.INTEGER,
      unique: 'photo_tag_tagging'
    },
    photoId: {
      type: DataTypes.INTEGER,
      unique: 'photo_tag_tagging'    
    }
  });
  
  return Tagging;
};
