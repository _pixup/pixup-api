module.exports = (sequelize, DataTypes) => {
  const Photo = sequelize.define('Photo', {
    cloudinary_id: DataTypes.STRING,
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    url: DataTypes.STRING,
    location: DataTypes.STRING,
    status: DataTypes.ENUM('APPROVED', 'UNAPPROVED'),
    metadata: DataTypes.TEXT,
  });
  Photo.associate = (models) => {
    Photo.hasMany(models.Like, {
      foreignKey: 'photoId',
      as: 'Likes'
    });

    Photo.belongsToMany(models.Tag, {
      through: {
        model: models.Tagging,
        unique: false
      },
      foreignKey: 'photoId',
      constraints: false,
      as: 'Tags'
    });

    Photo.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'Uploader'
    });
  };

  return Photo;
};
