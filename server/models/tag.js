module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define('Tag', {
    tagName: DataTypes.STRING
  });
  Tag.associate = (models) => {
    Tag.belongsToMany(models.Photo, {
      through: {
        model: models.Tagging,
        unique: false
      },
      foreignKey: 'tagId',
      constraints: false,
      as: 'Photos'
    })
  };
  return Tag;
};
