const {
  User,
  Photo,
  Like,
  Tagging,
  Tag
} = require('../models');

const { Op } = require('sequelize');

export default class searchController {
  static query(request, response) {
    const { query } = request.params;
    if (query.length > 54) {
      return response.status(401).json({
        status: 'error',
        message: 'Query is too long'
      });
    }

    const tagFilter = query.split(' ').reduce((acc, word) => {
      acc.push({
        tagName: { [Op.like]: `%${word}%` }
      });
      return acc;
    }, []);

    const photoFilter = query.split(' ').reduce((acc, word) => {
      acc.push({
        title: { [Op.like]: `%${word}%` }
      });
      acc.push({
        description: { [Op.like]: `%${word}%` }
      });
      return acc;
    }, []);

    const promises = [];
    // find photos by tags
    promises.push(Photo.findAll({
      include: [{
        model: User,
        as: 'Uploader',
        attributes: ['firstname', 'lastname', 'id']
      },
      {
        model: Like,
        as: 'Likes',
        include: [{
          model: User,
          attributes: ['firstname', 'lastname']
        }],
        attributes: ['id'],
      },
      {
        model: Tag,
        as: 'Tags',
        attributes: ['id', 'tagName'],
        where: {
          [Op.or]: tagFilter
        }
      }
      ],
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      }
    }));

    // search photo title and description
    promises.push(Photo.findAll({
      include: [{
        model: User,
        as: 'Uploader',
        attributes: ['firstname', 'lastname', 'id']
      },
      {
        model: Like,
        as: 'Likes',
        include: [{
          model: User,
          attributes: ['firstname', 'lastname']
        }],
        attributes: ['id'],
      },
      {
        model: Tag,
        as: 'Tags',
        attributes: ['id', 'tagName']
      }
      ],
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      },
      where: {
        [Op.or]: photoFilter
      }
    }));

    Promise.all(promises).then((results) => {
      const totalResults = [...results[0], ...results[1]];

      
      const uniqueResults = totalResults.filter((obj, pos, arr) => {
        //get an array of IDs and check if the indices of the results are equal to the index of its id in the id array
        return arr.map(mapObj => mapObj.id).indexOf(obj.id) === pos;
      });
      response.status(200).json({
        status: 'success',
        photos: uniqueResults
      });
    }).catch((error) => {
      response.status(500).json({
        status: 'error',
        message: error.toString()
      });
    });
  }
}
