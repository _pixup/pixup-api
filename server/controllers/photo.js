import { checkAdmin } from '../helpers/pixHelper';
import tagHelper from '../helpers/tagHelper';

const User = require('../models').User;
const Photo = require('../models').Photo;
const Like = require('../models').Like;
const Tagging = require('../models').Tagging;
const Tag = require('../models').Tag;


export default class photoController {
  static upload(request, response) {
    // check for photo url if it exists
    Photo.find({
      where: {
        url: request.body.photo_url
      }
    }).then((photo) => {
      if (photo) {
        return response.status(404).json({
          status: 'Unsuccessful',
          message: 'Photo with that link already exists',
        });
      }

      Photo
        .create({
          title: request.body.title,
          description: request.body.description,
          userId: request.loggedInUser.id,
          url: request.body.photo_url,
          status: 'UNAPPROVED',
          metadata: request.body.metadata,
          cloudinary_id: request.body.cloudinary_id,
          location: request.body.location
        })
        .then((newPix) => {
          tagHelper.tag(request, response, newPix.id, () => {
            console.log('new pix created, creating tags');
            return Photo
              .findById(newPix.id, {
                include: [{
                  model: User,
                  as: 'Uploader',
                  attributes: ['firstname', 'lastname']
                },
                {
                  model: Like,
                  as: 'Likes',
                  include: [{
                    model: User,
                    attributes: ['firstname', 'lastname']
                  }],
                  attributes: ['id'],
                },
                {
                  model: Tags,
                  as: 'Tags',
                  attributes: ['id','tagName'],
                }
                ],
                attributes: {
                  exclude: ['createdAt', 'updatedAt', 'deletedAt']
                },
              })
              .then((photo) => {
                console.log('photo about to send');
                return response.status(200).send(photo);
              })
              .catch(error => response.status(500).send(error.toString()));
          });
        })
        .catch(error => response.status(500).send(error.toString()));
    });
  }

  static download(request, response) {
    const photoId = parseInt(request.params.id);
    Photo
      .findById(photoId, {
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        },
      })
      .then((photo) => {
        return response.status(200).send({
          message: 'successful',
          url: photo.link
        });
      })
      .catch(error => response.status(500).send(error.toString()));
  }

  static allPhotos(request, response) {
    Photo.findAll({
      include: [{
        model: User,
        as: 'Uploader',
        attributes: ['firstname', 'lastname', 'id']
      },
      {
        model: Like,
        as: 'Likes',
        include: [{
          model: User,
          attributes: ['firstname', 'lastname']
        }],
        attributes: ['id'],
      },
      {
        model: Tag,
        as: 'Tags',
        attributes: ['id','tagName'],
      }
      ],
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'deletedAt']
      },
    })
      .then((photos) => {
        if (photos.length > 0) {
          response.status(200).json({
            status: 'Successful',
            data: photos
          });
        } else {
          response.status(200).json({
            status: 'Successful Access',
            message: 'but no photos yet'
          });
        }
      })
      .catch(error => response.status(500).send(error.toString()));
  }

  static onePhoto(request, response) {
    const photoId = parseInt(request.params.id);
    return Photo
      .findById(photoId, {
        include: [{
          model: User,
          as: 'Uploader',
          attributes: ['firstname', 'lastname']
        },
        {
          model: Like,
          as: 'Likes',
          include: [{
            model: User,
            attributes: ['firstname', 'lastname']
          }],
          attributes: ['id'],
        },
        {
          model: Tag,
          as: 'Tags',
          attributes: ['id','tagName']
        }
        ],
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        },
      })
      .then((photo) => {
        return response.status(200).send(photo);
      })
      .catch(error => response.status(500).send(error.toString()));
  }

  static approvePhoto(request, response) {
    const photoId = parseInt(request.params.id);
    Photo
      .findById(photoId)
      .then((photo) => {
        if (photo) {
          if (checkAdmin) {
            return Photo
              .update({
                status: 'APPROVED'
              })
              .then((approvedPix) => {
                response.status(200).send({
                  message: 'Pix approved',
                  pix: approvedPix
                });
              })
              .catch(error => response.status(500).send(error.toString()));
          }
        }
        return response.status(404).json({
          status: 'Unsuccessful',
          message: 'Pix not found',
        });
      })
      .catch(error => response.status(500).send(error.toString()));
  }

  static unapprovePhoto(request, response) {
    const photoId = parseInt(request.params.id);
    Photo
      .findById(photoId)
      .then((photo) => {
        if (photo) {
          if (checkAdmin) {
            return Photo
              .update({
                deletedAt: Date.now
              })
              .then((unapprovedPix) => {
                response.status(200).send({
                  message: 'Pix removed',
                  pix: unapprovedPix
                });
              })
              .catch(error => response.status(500).send(error.toString()));
          }
        }
        response.status(404).json({
          status: 'Unsuccessful',
          message: 'Pix not found',
        });
      })
      .catch(error => response.status(500).send(error.toString()));
  }

  static deletePhoto(request, response) {
    const photoId = parseInt(request.params.id);
    Photo
      .findById(photoId)
      .then((photo) => {
        if (photo) {
          if (checkAdmin) {
            return Photo
              .destroy()
              .then((deletedPix) => {
                response.status(200).send({
                  message: 'Pix successfully deleted',
                  pix: deletedPix
                });
              })
              .catch(error => response.status(500).send(error.toString()));
          }
        }
        response.status(404).json({
          status: 'Unsuccessful',
          message: 'Pix not found',
        });
      })
      .catch(error => response.status(500).send(error.toString()));
  }

  static likePhoto(request, response) {
    const photoId = parseInt(request.params.id);
    Photo
      .findById(photoId)
      .then((photo) => {
        if (photo) {
          return Like
            .find({
              where: {
                userId: request.loggedInUser.id,
                photoId
              }
            })
            .then((like) => {
              if (like) {
                return response.status(200).send({
                  message: 'Photo already liked by that user'
                });
              }

              return Like.create({
                userId: request.loggedInUser.id,
                photoId
              })
                .then((newLike) => {
                  response.status(200).send({
                    message: 'Success',
                    like: newLike
                  });
                })
                .catch(error => response.status(500).send(error.toString()));
            })
            .catch(error => response.status(500).send(error.toString()));
        }
        response.status(404).json({
          status: 'Unsuccessful',
          message: 'Pix not found',
        });
      })
      .catch(error => response.status(500).send(error.toString()));
  }
}
