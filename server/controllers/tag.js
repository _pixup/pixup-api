const {
  User,
  Photo,
  Like,
  Tagging,
  Tag
} = require('../models');

const { Op } = require('sequelize');

export default class TagController {
  static photos(request, response) {
    const { name } = request.params;
    Tag.find({
      include: [{
        model: Photo,
        as: 'Photos',
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        },
        include: [{
          model: User,
          as: 'Uploader',
          attributes: ['firstname', 'lastname']
        },
        {
          model: Like,
          as: 'Likes',
          include: [{
            model: User,
            attributes: ['firstname', 'lastname']
          }],
          attributes: ['id'],
        }
        ]
      }],
      where: {
        tagName: name
      }
    }).then((tag) => {
      if (tag.Photos.length > 0) {
        response.status(200).json({
          status: 'Successful',
          tag
        });
      } else {
        response.status(200).json({
          status: 'Success',
          tag,
          message: 'No photos found'
        });
      }
    })
      .catch(error => response.status(500).send(error.toString()));
  }

  static popular(request, response) {
    Tag.find({
      include: [{
        model: Photo,
        as: 'Photos',
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        }
      }],
      where: {
        tagName: name
      }
    }).then((tag) => {
      if (tag.Photos.length > 0) {
        response.status(200).json({
          status: 'Successful',
          tag
        });
      } else {
        response.status(200).json({
          status: 'Success',
          tag,
          message: 'No photos found'
        });
      }
    })
      .catch(error => response.status(500).send(error.toString()));
  }
}
