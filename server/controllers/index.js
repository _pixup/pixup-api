import UserController from './user';
import PhotoController from './photo';
import SearchController from './search';

// Export controllers
export default {
  UserController,
  PhotoController,
  SearchController
};
