require('dotenv').config();

module.exports = {
  development: {
    username: 'root',
    password: null,
    database: 'pix-up',
    host: '127.0.0.1',
    dialect: 'mysql', 
    operatorsAliases: false
  },
  staging: {
    username: 'gyuwnzkfuocokc',
    password: 'f590ea56d74d7f9235cb675f76472b81425dc1e1781869bc04563afb4427962c',
    database: 'd4mki19m32ns0v',
    host: 'ec2-184-73-250-50.compute-1.amazonaws.com',
    dialect: 'postgres',
    port: 5432,
    operatorsAliases: false
  }
};
