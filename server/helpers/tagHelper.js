const Tagging = require('../models').Tagging;
const Tag = require('../models').Tag;

export default class tagHelper {
  static tag(request, response, photoId, cb) {
    const tags = request.body.tags || [];

    const tagging = tags.map((tag, index) => {
      Tag.findOne({
        where: { tagName: tag.toLowerCase() }
      })
        .then((foundTag) => {

          // if tag exists add, tag the photo
          if (foundTag) {
            console.log('tag_found ====>>>>>>>>>>>');
            console.log(foundTag);
            return Tagging.create({
              tagId: foundTag.id,
              photoId
            }).then((newTagging) => {
              
              console.log('tagging created');
              return newTagging;
            }).catch(error => console.log(error));
          }

          // else create the tag, the tag the photo
          return Tag.create({
            tagName: tag.toLowerCase()
          }).then((newTag) => {
            return Tagging.create({
              tagId: newTag.id,
              photoId
            }).then((newTagging) => {
              
              return newTagging;
            });
          });
        });
    });
    cb();
  }
}
